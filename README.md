# WHAT-TO-DO
Simple JSP program that tells user what to do based on the time of the day.

Requirements:  
- Java  
- Tomcat

### To set up:
> 1. git clone https://_ismayil@bitbucket.org/_ismayil/what-to-do.git
> 2. Move the file what-to-do/whattodo.jsp under Tomcat
> 3. Start Tomcat
> 3. Navigate to http://localhost:8080/whattodo.jsp
